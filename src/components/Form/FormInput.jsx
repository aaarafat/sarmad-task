import React from 'react';

export default function FormInput({ id, label, value, handleChange }) {
  return (
    <div className='form__group'>
      <label className='form-label' htmlFor={id}>
        {label}
      </label>
      <input
        className='form-control'
        type='text'
        id={id}
        name={id}
        value={value}
        onChange={(e) => handleChange(e)}
      />
    </div>
  );
}
