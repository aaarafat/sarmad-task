import { useState, useCallback } from 'react';
import { debounce } from 'lodash';
import FormInput from './FormInput';
export default function Form({ callback, loading, error, reset }) {
  const [data, setData] = useState({
    fname: '',
    mname: '',
    lname: '',
    nat: '',
  });
  const [formDirty, setFormDirty] = useState(false);

  const debouncedCallback = useCallback(debounce(callback, 300), []);
  const canSubmit = (payload) => {
    return (
      payload.fname.length > 1 ||
      payload.mname.length > 1 ||
      payload.lname.length > 1 ||
      payload.nat.length > 1
    );
  };

  const handleReset = () => {
    setData({
      fname: '',
      mname: '',
      lname: '',
      nat: '',
    });
    setFormDirty(false);
    reset();
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!canSubmit(data)) return;

    callback(data);
  };
  const handleChange = (e) => {
    const payload = { ...data, [e.target.name]: e.target.value };
    setData(payload);
    setFormDirty(true);
    if (!canSubmit(payload)) return;
    debouncedCallback(payload);
  };
  return (
    <>
      <form className='form' onSubmit={handleSubmit}>
        <div className='form__inputs'>
          <FormInput
            id='fname'
            label='First Name'
            value={data.fname}
            handleChange={handleChange}
          />
          <FormInput
            id='mname'
            label='Middle Name'
            value={data.mname}
            handleChange={handleChange}
          />
          <FormInput
            id='lname'
            label='Last Name'
            value={data.lname}
            handleChange={handleChange}
          />
          <FormInput
            id='nat'
            label='Nationality'
            value={data.nat}
            handleChange={handleChange}
          />
        </div>
        {formDirty && !canSubmit(data) && (
          <div className='text-dim fw-light mb-3'>
            Please enter at least 2 characters on at least one search criteria
          </div>
        )}
        {formDirty && error && (
          <div className='text-danger fw-light mb-3'>
            An error has occured please try again
          </div>
        )}
        <div className='d-flex justify-content-between'>
          <button
            className='btn btn-secondary'
            type='submit'
            disabled={loading || !canSubmit(data)}
          >
            {loading ? 'Loading...' : 'Search'}
          </button>
          <button
            onClick={handleReset}
            className='btn btn-primary'
            type='button'
            disabled={loading}
          >
            Clear
          </button>
        </div>
      </form>
    </>
  );
}
