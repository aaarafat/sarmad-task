import { useEffect, useState, useMemo } from 'react';
import { usePagination, useSortBy, useTable } from 'react-table';
import Paginator from './Paginator';

export default function Table({ result }) {
  const columns = useMemo(
    () => [
      {
        Header: 'Name',
        accessor: 'name',
        disableSortBy: result.length ? false : true,
      },
      {
        Header: 'Score',
        accessor: 'score',
        disableSortBy: result.length ? false : true,
      },
      {
        Header: 'Nationality',
        accessor: 'nationality',
        disableSortBy: result.length ? false : true,
      },
      {
        Header: 'Place of Birth',
        accessor: 'placeOfBirth',
        disableSortBy: result.length ? false : true,
      },
      {
        Header: 'Description',
        accessor: 'description',
        disableSortBy: result.length ? false : true,
      },
    ],
    [result]
  );
  const data = useMemo(() => result, [result]);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable({ columns, data }, useSortBy, usePagination);

  useEffect(() => {
    setPageSize(15);
  }, []);

  const sortIcon = (column) => (
    <div className='me-1'>
      {column.isSorted ? (
        column.isSortedDesc ? (
          <i className='fa-solid fa-chevron-down'></i>
        ) : (
          <i className='fa-solid fa-chevron-up'></i>
        )
      ) : (
        ''
      )}
    </div>
  );

  return (
    <>
      <table {...getTableProps()} className='table'>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                  <div className='d-flex justify-content-between'>
                    <div>{column.render('Header')}</div>
                    {sortIcon(column)}
                  </div>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((row, i) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
      {!data.length && (
        <div className='text-dim d-flex justify-content-center align-items-center'>
          <i className='fa-solid fa-circle-info me-2'></i>
          No records to display
        </div>
      )}

      {data.length ? (
        <Paginator
          {...{
            canPreviousPage,
            canNextPage,
            gotoPage,
            nextPage,
            previousPage,
            pageIndex,
            pageOptions,
            pageCount,
          }}
        />
      ) : null}
    </>
  );
}
