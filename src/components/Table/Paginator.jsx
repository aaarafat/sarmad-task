import { useEffect, useState } from 'react';

export default function Paginator({
  canPreviousPage,
  canNextPage,
  gotoPage,
  nextPage,
  previousPage,
  pageIndex,
  pageOptions,
  pageCount,
}) {
  const [maxPages, setMaxPages] = useState(4);
  const [minPages, setMinPages] = useState(0);
  const [pages, setPages] = useState([]);
  useEffect(() => {
    if (pageIndex == 0) {
      setMinPages(0);
      setMaxPages(4);
    }
    const _pages = [];
    for (let i = 0; i < pageCount; i++) {
      _pages.push(i);
    }
    setPages(_pages);
  }, [pageCount, pageIndex]);

  const handleNext = () => {
    if (pageIndex == maxPages) {
      const _max = Math.min(pageIndex + 5, pageCount - 1);
      const _min = Math.max(_max - 4, 0);
      setMaxPages(_max);
      setMinPages(_min);
    }
    nextPage();
  };
  const handlePrevious = () => {
    if (pageIndex == minPages) {
      const _min = Math.max(pageIndex - 5, 0);
      const _max = Math.min(_min + 4, pageCount - 1);
      setMinPages(_min);
      setMaxPages(_max);
    }
    previousPage();
  };
  const handleGotoPage = (index) => {
    if (index >= maxPages) {
      let _max = Math.min(index + 4, pageCount - 1);
      let _min = _max - 4;

      setMinPages(_min);
      setMaxPages(_max);
    } else if (index <= minPages) {
      let _min = Math.max(index - 4, 0);
      let _max = _min + 4;
      setMinPages(_min);
      setMaxPages(_max);
    }
    gotoPage(index);
  };

  return (
    <div className='paginator d-flex justify-content-between align-items-center'>
      <div>
        <nav aria-label='Page navigation'>
          <ul className='pagination'>
            <li
              className={'page-item ' + (canPreviousPage ? '' : 'disabled')}
              onClick={() => handleGotoPage(0)}
            >
              <a className='page-link' href='#'>
                <i className='fa-solid fa-angles-left'></i>
              </a>
            </li>
            <li
              className={'page-item ' + (canPreviousPage ? '' : 'disabled')}
              onClick={() => handlePrevious()}
            >
              <a className='page-link' href='#'>
                <i className='fa-solid fa-angle-left'></i>
              </a>
            </li>
            {pages.map((page, index) => {
              if (index >= minPages && index <= maxPages) {
                return (
                  <li
                    key={index}
                    className={
                      'page-item page-index ' +
                      (pageIndex == index ? 'active' : '')
                    }
                    onClick={() => handleGotoPage(index)}
                  >
                    <a className='page-link' href='#'>
                      {index + 1}
                    </a>
                  </li>
                );
              }
            })}

            <li
              onClick={() => handleNext()}
              className={'page-item ' + (canNextPage ? '' : 'disabled')}
            >
              <a className='page-link' href='#'>
                <i className='fa-solid fa-angle-right'></i>
              </a>
            </li>
            <li
              className={'page-item ' + (canNextPage ? '' : 'disabled')}
              onClick={() => handleGotoPage(pageCount - 1)}
            >
              <a className='page-link' href='#'>
                <i className='fa-solid fa-angles-right'></i>
              </a>
            </li>
          </ul>
        </nav>
      </div>
      <div>
        <span className='text-secondary'>
          Page
          <strong>{` ${pageIndex + 1} of ${pageCount}`}</strong>
        </span>
      </div>
    </div>
  );
}
