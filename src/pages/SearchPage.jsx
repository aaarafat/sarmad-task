import Form from '../components/Form/Form';
import Table from '../components/Table/Table';
import useSearchAPI from '../api/search-api';

function SearchPage() {
  const [result, loading, error, post, reset] = useSearchAPI();

  return (
    <div className='search'>
      <div className='wrapper'>
        <Form callback={post} loading={loading} error={error} reset={reset} />
      </div>
      <div className='wrapper'>
        <Table result={result} />
      </div>
    </div>
  );
}

export default SearchPage;
