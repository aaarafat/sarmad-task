import { useState, useCallback, useMemo } from 'react';
import axios from 'axios';
import { searchResultMapper } from '../utils/search-utilis';

const HEADERS = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
};

const ENDPOINT =
  'http://150.230.49.47:8080/api/v1/integration/focal/screen/individual';

const SearchAPIServie = {
  post: (payload) => {
    return axios.post(ENDPOINT, payload, { headers: HEADERS });
  },
};

export default function useSearchAPI() {
  const [result, setResult] = useState([]);
  const [fetchers, setFetchers] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const memoizedFetchers = useMemo(() => fetchers, [fetchers]);
  const reset = useCallback(() => {
    setResult([]);
    setFetchers([]);
    setLoading(false);
    setError(null);
  });
  const post = useCallback((payload) => {
    setFetchers((prev) => [...prev, payload]);
    setLoading(true);
    SearchAPIServie.post(payload)
      .then(({ data: { screen_result } }) => {
        setResult(searchResultMapper(screen_result));
        setError(null);
      })
      .catch((err) => {
        setError(err);
        console.log('Error: ', err.message);
        setResult([]);
      })
      .finally(() => {
        setFetchers(fetchers.filter((f) => f !== payload));
        setLoading(memoizedFetchers.length > 0);
      });
  });

  return [result, loading, error, post, reset];
}
