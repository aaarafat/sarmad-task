export const searchResultMapper = (search_result) => {
  return search_result.map((item, index) => {
    return {
      id: index,
      name: item.name,
      description: item.search_types.length
        ? item.search_types[0].description
        : '-',
      nationality: item.nat,
      placeOfBirth: item.places_of_birth.length ? item.places_of_birth[0] : '-',
      score: item.score,
    };
  });
};
