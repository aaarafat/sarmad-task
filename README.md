# Sarmad task
## Try it [here](https://sarmad-task.vercel.app/)
#### **The API is deployed on an unsecure server `http` so any deployment on a secure server `https` won't be able to communicate with this API**
#### `vercel` only allows secure connection so the deployed app above won't be able to fetch data
#### To fully try the app follow the steps mentioned below 
## Project Setup
### Clone the repo
### Install dependencies
```sh
npm install
```

### To run the App
```sh
npm run dev
```
## Screenshots
![image](https://user-images.githubusercontent.com/44725090/231283515-411b02b5-d2f6-40d0-addc-042cf464accd.png)
![image](https://user-images.githubusercontent.com/44725090/231284299-023def0e-2ae3-4e52-a0b1-aa982749273d.png)
![image](https://user-images.githubusercontent.com/44725090/231285095-7db7cd4a-7752-428b-95aa-82a1f24d3fb9.png)
![image](https://user-images.githubusercontent.com/44725090/231284981-953bd0d3-27c9-4cc9-8ffb-5243a699406c.png)

## Project Structure
```
├─ public
│  └─ images
│     └─ sarmad.svg
├─ src
│  ├─ main.jsx
│  ├─ App.jsx
│  ├─ api
│  │  └─ search-api.jsx
│  ├─ components
│  │  ├─ Form
│  │  │  ├─ Form.jsx
│  │  │  └─ FormInput.jsx
│  │  └─ Table
│  │     ├─ Paginator.jsx
│  │     └─ Table.jsx
│  ├─ pages
│  │  └─ SearchPage.jsx
│  ├─ scss
│  │  ├─ abstracts
│  │  │  ├─ _mixins.scss
│  │  │  └─ _variables.scss
│  │  ├─ base
│  │  │  ├─ _global.scss
│  │  │  ├─ _overrides.scss
│  │  │  └─ _typography.scss
│  │  ├─ components
│  │  │  ├─ form.scss
│  │  │  ├─ paginator.scss
│  │  │  └─ table.scss
│  │  ├─ main.scss
│  │  └─ pages
│  │     └─ search.scss
│  └─ utils
│     └─ search-utilis.js
└─index.html
```



